psql -U postgres

DROP DATABASE IF EXISTS basedatos_tp;

CREATE DATABASE basedatos_tp;

\c basedatos_tp;






CREATE TABLE Cliente(

 numero_cliente int NOT NULL,

 telefono int NOT NULL, 

 nombre TEXT NOT NULL, 

 apellido TEXT NOT NULL, 

 direccion TEXT NOT NULL,

 dni int NOT NULL);






CREATE TABLE Tarjeta(

 numero_tarjeta int NOT NULL,

 limite_compra int NOT NULL,

 fecha_vencimiento DATE NOT NULL,

 estado_tarjeta TEXT NOT NULL,

 codigo_seguridad int NOT NULL,

 numero_cliente int);




CREATE TABLE Comercio(

 id_comercio int NOT NULL, 

 nombre TEXT NOT NULL, 

 codigo_postal int NOT NULL);





CREATE TABLE Resumen(

 id_resumen int NOT NULL, 

 periodo_resumen DATE, 

 fecha_vencimiento DATE,
 numero_tarjeta int);





CREATE TABLE Compra(

 id_compra int, 

 numero_tarjeta int,

 codigo_seguridad_tarjeta int, 

 fecha_hora DATE, 

 monto int,

 id_comercio int NOT NULL, 

 id_resumen int NOT NULL);
 
 
 
 CREATE TABLE CompraInvalida(

 id_compra int, 

 numero_tarjeta int,

 codigo_seguridad_tarjeta int, 

 fecha_hora DATE, 

 monto int);



CREATE TABLE Alerta(

 id_alerta int NOT NULL, 

 descripcion_alerta TEXT,

 id_compra int NOT NULL);



ALTER TABlE Cliente ADD CONSTRAINT pk_cliente PRIMARY KEY (numero_cliente, dni);

ALTER TABlE Tarjeta ADD CONSTRAINT pk_tarjeta PRIMARY KEY (numero_tarjeta);

ALTER TABlE Comercio ADD CONSTRAINT pk_comercio PRIMARY KEY(id_comercio);

ALTER TABlE Resumen ADD CONSTRAINT pk_resumen PRIMARY KEY(id_resumen);

ALTER TABlE Compra ADD CONSTRAINT pk_compra PRIMARY KEY (id_compra, numero_tarjeta);

ALTER TABlE Alerta ADD CONSTRAINT pk_alerta PRIMARY KEY(id_alerta);



ALTER TABLE Tarjeta ADD FOREIGN KEY (numero_cliente) REFERENCES Cliente(numero_cliente);

ALTER TABLE Compra ADD FOREIGN KEY (numero_tarjeta) REFERENCES Cliente(numero_tarjeta);

ALTER TABLE Compra ADD FOREIGN KEY (id_comercio) REFERENCES Comercio(id_comercio);

ALTER TABLE Alerta ADD FOREIGN KEY (id_compra) REFERENCES Compra(id_compra);

ALTER TABLE Resumen ADD FOREIGN KEY (numero_tarjeta) REFERENCES Tarjeta(numero_tarjeta);