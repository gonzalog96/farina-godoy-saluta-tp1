﻿-- borrar la base de datos si existe
DROP DATABASE IF EXISTS basedatos_tp;
-- creación de la base de datos.
CREATE DATABASE basedatos_tp;

-- conexión contra la base de datos creada.
\c basedatos_tp;

-- creamos una nueva tabla Cliente.
-- posee un dni, telefono, nombre, apellido y direccion.
CREATE TABLE Cliente
(
 dni int NOT NULL,
 nombre varchar(32) NOT NULL, 
 apellido varchar(32) NOT NULL, 
 direccion TEXT NOT NULL,
 telefono int NOT NULL
);

-- creamos la tabla Tarjeta.
-- una tarjeta posee un número, un código de seguridad, un límite de compra, una fecha de vencimiento, un estado actual y está vinculada a un cliente en particular.
CREATE TABLE Tarjeta
(
 numero_tarjeta int NOT NULL,
 codigo_seguridad int NOT NULL,
 limite_compra int NOT NULL,
 fecha_vencimiento DATE NOT NULL,
 estado_tarjeta varchar(12) NOT NULL,
 id_cliente int NOT NULL
);

-- creamos la tabla Comercio.
-- un comercio está compuesto por su ID, un nombre y su código postal.
CREATE TABLE Comercio
(
 id_comercio int NOT NULL, 
 nombre varchar(32) NOT NULL, 
 codigo_postal int NOT NULL
);

-- creación de la tabla Resumen.
-- un resumen está compuesto por un identificador, un período, una fecha de vencimiento y una tarjeta asociada.
CREATE TABLE Resumen
(
 id_resumen int NOT NULL, 
 periodo_resumen DATE, 
 fecha_vencimiento DATE,
 id_tarjeta int NOT NULL
);

-- creación de la tabla Compra.
-- esta tabla posee un identificador, un monto, una fecha y hora de compra y un id de tarjeta y comercio asociados.
CREATE TABLE Compra
(
 id_compra int NOT NULL,
 monto int NOT NULL,
 fecha_hora DATE NOT NULL, 
 tarjeta_id int NOT NULL,
 comercio_id int NOT NULL 
);

-- tabla Alerta.
-- posee un identificador, una descripción con el tipo de alerta asociada y un id de compra asociado.
CREATE TABLE Alerta
(
 id_alerta int NOT NULL, 
 descripcion_alerta varchar(64) NOT NULL,
 compra_id int NOT NULL
);

-- tabla Compra_invalida.
-- esta tabla soporta el almacenamiento de las compras invalidas, pertenecientes a tarjetas inexistentes en la DB o debido a su estado de suspension o fuera del limite de compra.
CREATE TABLE Compra_invalida
(
 id_compra_inval int NOT NULL,
 tarjeta_invalida_id int NOT NULL,
 fecha_compra_inval DATE NOT NULL
);

-- creacion de las primary keys.
ALTER TABLE Cliente ADD PRIMARY KEY(dni);
ALTER TABLE Tarjeta ADD PRIMARY KEY(numero_tarjeta);
ALTER TABLE Compra ADD PRIMARY KEY(id_compra);
ALTER TABLE Comercio ADD PRIMARY KEY(id_comercio);
ALTER TABLE Resumen ADD PRIMARY KEY(id_resumen);
ALTER TABLE Alerta ADD PRIMARY KEY(id_alerta);
ALTER TABLE Compra_invalida ADD PRIMARY KEY(id_compra_inval);

-- creacion de las foreign keys.
ALTER TABLE Tarjeta ADD CONSTRAINT tarjeta_fk FOREIGN KEY(id_cliente) REFERENCES Cliente(dni);
ALTER TABLE Resumen ADD CONSTRAINT resumen_fk FOREIGN KEY(id_tarjeta) REFERENCES Tarjeta(numero_tarjeta);
ALTER TABLE Compra ADD CONSTRAINT compra_fk1 FOREIGN KEY(tarjeta_id) REFERENCES Tarjeta(numero_tarjeta);
ALTER TABLE Compra ADD CONSTRAINT compra_fk2 FOREIGN KEY(comercio_id) REFERENCES Comercio(id_comercio);

